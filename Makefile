all: results/hs37d5/ALU.final_comp.vcf

results/hs37d5/ALU.final_comp.vcf: code/MELTv2.1.5/MELT.jar data/raw/hs37d5.fa data/raw/NA12878.mapped.ILLUMINA.bwa.CEU.low_coverage.20121211.bam
	mkdir -p results/hs37d5
	java -jar code/MELTv2.1.5/MELT.jar Single -a -b hs37d5/NC_007605 -c 8 -h data/raw/hs37d5.fa -bamfile data/raw/NA12878.mapped.ILLUMINA.bwa.CEU.low_coverage.20121211.bam -n code/MELTv2.1.5/hg19.genes.bed -t code/MELTv2.1.5/me_refs/Hg38/ALU_MELT.zip -w results/hs37d5

data/raw/NA12878.mapped.ILLUMINA.bwa.CEU.low_coverage.20121211.bam:
	mkdir -p data/raw
	wget --directory-prefix=data/raw ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/phase3/data/NA12878/alignment/NA12878.mapped.ILLUMINA.bwa.CEU.low_coverage.20121211.bam
	wget --directory-prefix=data/raw ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/phase3/data/NA12878/alignment/NA12878.mapped.ILLUMINA.bwa.CEU.low_coverage.20121211.bam.bai

data/raw/hs37d5.fa:
	mkdir -p data/raw
	wget --directory-prefix=data/raw ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/reference/phase2_reference_assembly_sequence/hs37d5.fa.gz
	wget --directory-prefix=data/raw ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/reference/phase2_reference_assembly_sequence/hs37d5.fa.gz.fai
	gunzip data/raw/hs37d5.fa.gz
	mv data/raw/hs37d5.fa.gz.fai data/raw/hs37d5.fa.fai
